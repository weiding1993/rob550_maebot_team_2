# PID.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm
from math import *

class PID():
    def __init__(self, kp, ki, kd, iTerm):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.iTerm = iTerm
        self.iTermMin = -0.6
        self.iTermMax = 0.6
        self.prevInput = 0.0
        self.input = 0.0
        self.output = 0.0
        self.setpoint = 0.0
        self.outputMin = -0.6
        self.outputMax = 0.6
        self.updateRate = 0.1

    def Compute(self):
	      #calc error
        error = self.setpoint - self.input
        
        # calc iTerm
        self.iTerm += self.ki * error 
        if(self.iTerm > self.iTermMax):
            self.iTerm = self.iTermMax
        elif(self.iTerm < self.iTermMin):
            self.iTerm= self.iTermMin
        #calc dTerm
        dTerm = self.kd*(self.input - self.prevInput)
        
        self.output = self.kp * error + self.iTerm + dTerm
        
        if(self.output > self.outputMax):
            self.output = self.outputMax
        elif(self.output < self.outputMin):
            self.output= self.outputMin

        #print error,self.iTerm,self.output

    def SetTunings(self, kp, ki, kd):
        self.kp = kp
        self.ki = ki * self.updateRate
        self.kd = kd / self.updateRate

    def SetIntegralLimits(self, imin, imax):
        self.iTermMinMax = imin
        self.iTermMaxMax = imax

    def SetOutputLimits(self, outmin, outmax):
        self.outputMin = outmin
        self.outputMax = outmax
        
    def SetUpdateRate(self, rateInSec):
        self.updateRate = rateInSec
