# robot.py
#
# provides Maebot class
#
# for University of Michigan ROB550 BotLab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from breezyslam.robots import WheeledRobot
from math import pi, degrees

class Maebot(WheeledRobot):
    
    def __init__(self):
        self.wheelDiameterMillimeters = 32.0     # diameter of wheels [mm]
        self.axleLengthMillimeters = 80.0        # separation of wheels [mm]  
        self.ticksPerRev = 16.0             # encoder tickers per motor revolution
        self.gearRatio = 30.0		    # 30:1 gear ratio
        self.enc2mm = (pi * self.wheelDiameterMillimeters) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]
        self.prevEncPos = [0,0,0]           # store previous readings for odometry 
        WheeledRobot.__init__(self, self.wheelDiameterMillimeters/2.0, self.axleLengthMillimeters/2.0)

    def getVelocities(self, currEncPos, prevEncPos):
        # implement me! EncPos is in the form [left, right, timestamp]  
        dLeft = (currEncPos[0]-prevEncPos[0])
        dRight = (currEncPos[1]-prevEncPos[1])
        dt = (currEncPos[2]-prevEncPos[2])/1E6        
        dxy = self.enc2mm * (dRight + dLeft)/2
        dtheta = self.enc2mm * (dRight - dLeft)/self.axleLengthMillimeters
        self.prevEncPos = currEncPos

        return [dxy, dtheta, dt] # [mm], [rad], [s] 
