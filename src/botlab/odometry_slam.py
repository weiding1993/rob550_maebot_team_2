# odometry.py
#
# skeleton code for ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm
from math import *

from slam import *
from robot import *
from laser import *
from maps import *

from lcmtypes import maebot_sensor_data_t
from lcmtypes import maebot_motor_feedback_t 
from lcmtypes import pose_xyt_t
from lcmtypes import rplidar_laser_t

# SLAM preferences
USE_ODOMETRY = True
MAP_QUALITY = 3

# Laser constants
DIST_MIN = 100; # minimum distance
DIST_MAX = 6000; # maximum distance

# Map constants
MAP_SIZE_M = 20.0 # size of region to be mapped [m]
INSET_SIZE_M = 2.0 # size of relative map
MAP_RES_PIX_PER_M = 100 # number of pixels of data per meter [pix/m]
MAP_SIZE_PIXELS = int(MAP_SIZE_M*MAP_RES_PIX_PER_M) # number of pixels across the entire map
MAP_DEPTH = 5 # depth of data points on map (levels of certainty)

KWARGS, gvars = {}, globals()
for var in ['MAP_SIZE_M','INSET_SIZE_M','MAP_RES_PIX_PER_M','MAP_DEPTH','USE_ODOMETRY','MAP_QUALITY']:
  KWARGS[var] = gvars[var] # constants required in modules

lc = lcm.LCM()
robot = Maebot()
laser = RPLidar(DIST_MIN, DIST_MAX) # lidar
data = DataMatrix(**KWARGS) # handle map data
slam = Slam(self.robot, self.laser, **KWARGS) # do slam processing

def normalizeAngle(angle, (lower, upper)):
    while angle <= lower:
        angle += 360
    while angle >= upper:
        angle -= 360
    return angle

def motorFeedbackHandler(channel,data):
    msg = maebot_motor_feedback_t.decode(data)
    #process message here
    

def sensorDataHandler(channel,data):
    msg = maebot_sensor_data_t.decode(data)
    #process message here

def rplidarDataHandler(channel, data):
    msg = rplidar_laser_t.decode(data)
    points = [point for point in msg[1], range(msg[0])]
    if init: slam.prevEncPos = slam.currEncPos
    data.getRobotPos(slam.updateSlam(points), init=init)
    data.drawBreezyMap(slam.getBreezyMap())
    #process message here 

def main():
    lcmMotorSub = lc.subscribe("MAEBOT_MOTOR_FEEDBACK", motorFeedbackHandler)
    lcmSensorSub = lc.subscribe("MAEBOT_SENSOR_DATA", sensorDataHandler)
    lcmLidarSub = lc.subscribe("RPLIDAR_LASER", rplidarDataHandler)
    botPose = pose_xyt_t()
    while(1):
        lc.handle()
        #calculate pose here
	botPose.utime = int(time.time()*1E6)
	botPose.xyt[0] = 0
	botPose.xyt[1] = 0
	botPose.xyt[2] = 0
        lc.publish("BOTLAB_ODOMETRY",botPose.encode())
    lc.unsubscribe(lcmMotorSub)
    lc.unsubscribe(lcmSensorSub)

main()



