# gyrodometry.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm
from math import *

from robot import *

from lcmtypes import maebot_sensor_data_t
from lcmtypes import maebot_motor_feedback_t 
from lcmtypes import pose_xyt_t

lc = lcm.LCM()
robot = Maebot()

INIT = 0
prevPose = [0,0,0]
currPose = [0,0,0]

def normalizeAngle(angle, (lower, upper)):
    while angle <= lower:
        angle += 2*pi
    while angle >= upper:
        angle -= 2*pi
    return angle

def motorFeedbackHandler(channel,data):
    global INIT
    msg = maebot_motor_feedback_t.decode(data)
    #process message here
    timestamp = msg.utime
    leftTicks = msg.encoder_left_ticks
    rightTicks = msg.encoder_right_ticks
    currEncPos = [leftTicks, rightTicks, timestamp]
    if INIT != 1:
        robot.prevEncPos = currEncPos
        INIT = 1
    dpose = robot.getVelocities(currEncPos, robot.prevEncPos)
    dxy = dpose[0]
    dtheta = dpose[1]
    dt = dpose[2]
    prevPose = currPose
    currPose[2] = normalizeAngle(prevPose[2] + dtheta,(-pi, pi))
    currPose[0] = prevPose[0] + dxy * cos(currPose[2])
    currPose[1] = prevPose[1] + dxy * sin(currPose[2])
    #end process message
    
def sensorDataHandler(channel,data):
    msg = maebot_sensor_data_t.decode(data)
    #process message here

def main():
    lcmMotorSub = lc.subscribe("MAEBOT_MOTOR_FEEDBACK", motorFeedbackHandler)
    #lcmSensorSub = lc.subscribe("MAEBOT_SENSOR_DATA", sensorDataHandler)
    botPose = pose_xyt_t()
    INIT = 0
    while(1):
        lc.handle()
        #lc.handle()
        #calculate pose here
	botPose.utime = int(time.time()*1E6)
	botPose.xyt[0] = currPose[0]
	botPose.xyt[1] = currPose[1]
	botPose.xyt[2] = currPose[2]
        lc.publish("BOTLAB_ODOMETRY",botPose.encode())
    lc.unsubscribe(lcmMotorSub)
    #lc.unsubscribe(lcmSensorSub)

main()



