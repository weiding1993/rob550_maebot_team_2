# slam.py
# 
# provides Slam class
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from breezyslam.algorithms import RMHC_SLAM

def normalizeAngle(angle, (lower, upper)):
    while angle <= lower:
        angle += 360
    while angle >= upper:
        angle -= 360
    return angle


class Slam(RMHC_SLAM):
    def __init__(self, robot, laser, MAP_SIZE_M=8.0, MAP_RES_PIX_PER_M=100, MAP_QUALITY=5, HOLE_WIDTH_MM = 200, RANDOM_SEED = 0xabcd,  **unused):
        MAP_SIZE_PIXELS = int(MAP_SIZE_M*MAP_RES_PIX_PER_M) # number of pixels across the entire map
        RMHC_SLAM.__init__(self, \
                       laser, \
                       MAP_SIZE_PIXELS, \
                       MAP_SIZE_M, \
                       MAP_QUALITY, \
                       HOLE_WIDTH_MM, \
                       RANDOM_SEED)

        self.robot = robot
        self.scanSize = laser.SCAN_SIZE

        self.prevEncPos = () # previous robot encoder data
        self.currEncPos = () # left wheel [ticks], right wheel [ticks], timestamp [us]

        self.breezyMap = bytearray(MAP_SIZE_PIXELS**2) # initialize map array for BreezySLAM's internal mapping

    def getBreezyMap(self):
        return self.breezyMap

    def updateSlam(self, points):
        distVec = [0 for i in range(self.scanSize)]
        i = 0
        for point in points: # create breezySLAM-compatible data from raw scan data
            dist = point[0]
            index = int(point[1])
            if not 0 <= index < self.scanSize: continue
            distVec[index] = int(dist)
        self.update(distVec, self.getVelocities())
        x, y, theta = self.getpos()
        theta = normalizeAngle(theta, (-180.0,180.0))

        self.getmap(self.breezyMap) # write internal map to breezyMap
        return (y, x, theta)

    def getVelocities(self):
        velocities = self.robot.getVelocities(self.currEncPos, self.prevEncPos)
        self.prevEncPos = self.currEncPos
        return velocities
