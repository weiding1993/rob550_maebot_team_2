# PID.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time,signal
import lcm
from math import *
import PID

from lcmtypes import maebot_diff_drive_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import odo_pose_xyt_t

lc = lcm.LCM()

INIT_FLAG = 0 # flag initial read of encoder values


class PIDSpeedController():
  def __init__(self):
   
    # Create Both PID
    self.fwdVelCtrl = PID.PID(0.0000,0.000010,0,0.3)
    self.angCtrl = PID.PID(0.0,0.0,-0.25,0)
 
    # LCM Subscribe
    self.lc = lcm.LCM()
    lcmOdoVelSub = self.lc.subscribe("BOT_ODO_VEL", self.OdoVelocityHandler)
    lcmOdoPoseSub = self.lc.subscribe("BOT_ODO_POSE", self.OdoPositionHandler)
    self.frequency = 50.0
    signal.signal(signal.SIGINT, self.signal_handler)

  def publishMotorCmd(self,rightSpeed, leftSpeed):
    cmd = maebot_diff_drive_t()
    cmd.motor_right_speed = rightSpeed
    cmd.motor_left_speed = leftSpeed	
    self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())

  def OdoPositionHandler(self,channel,data): 
    odo_pose_xyt = odo_pose_xyt_t.decode(data)
    self.angCtrl.input = odo_pose_xyt.xyt[2]
    print odo_pose_xyt.utime

  def OdoVelocityHandler(self,channel,data):
    odo_dxdtheta = odo_dxdtheta_t.decode(data)
    if (odo_dxdtheta.dt > 0):
      self.fwdVelCtrl.input = odo_dxdtheta.dxy/odo_dxdtheta.dt

  def Controller(self):

    oldTime = time.time()

    self.fwdVelCtrl.setpoint = 200.0
    self.angCtrl.setpoint = 0.0
    self.angCtrl.SetOutputLimits(-0.05,0.05)

    while(1):
 
      if(time.time()-oldTime > 1.0/self.frequency):
        self.lc.handle()
        self.fwdVelCtrl.Compute()
        self.angCtrl.Compute()
        rightSpeed = self.fwdVelCtrl.output + self.angCtrl.output/2.0
        leftSpeed  = self.fwdVelCtrl.output - self.angCtrl.output/2.0

        print self.angCtrl.input, self.angCtrl.output
        sys.stdout.flush()
#        print leftSpeed, rightSpeed  

        self.publishMotorCmd(rightSpeed, leftSpeed)
        oldTime = time.time()
  
  def signal_handler(self,signal, frame):
    print("Terminating!")
    for i in range(5):
      cmd = maebot_diff_drive_t()
      cmd.motor_right_speed = 0.0
      cmd.motor_left_speed = 0.0	
      self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
    exit(1)

pid = PIDSpeedController()
pid.Controller()
